import random
from pygame.math import Vector2
import core

class Obstacle :
    def __init__(self,x,y,size = 20) :
        self.position =  Vector2(x,y)
        self.color = (random.randint(0,255),random.randint(0,255),random.randint(0,255))
        self.size = size

    def show(self) : 
        core.Draw.circle(self.color,self.position,self.size)
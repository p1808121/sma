from pygame.math import Vector2
import random

class Body :
    def __init__(self,fustrum,x,y):
        self.position = Vector2(x,y)
        self.vitesse = Vector2()
        self.acceleration = Vector2()
        self.vMax = 0.1
        self.accMax = 5
        self.fustrum = fustrum
        self.taille = 10
        self.color = (random.randint(0,255),random.randint(0,255),random.randint(0,255))

    def move (self,desision: Vector2):
        if desision.length()>self.accMax : 
            desision.scale_to_length(self.accMax)
        self.vitesse += desision

        if self.vitesse.length()>self.vMax :
            self.vitesse.scale_to_length(self.vMax)
        self.position += self.vitesse
    
    def size (self) :
        return self.taille
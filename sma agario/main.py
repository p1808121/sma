import random
from pygame.math import Vector2
import core
from agents import Agents
from body import Body
from fustrum import Fustrum
from obstacle import Obstacle
from creep import Creep

def setup():
    print("Setup START---------")
    core.fps = 30
    core.WINDOW_SIZE = [400, 400]

    core.memory("agents", [])
    core.memory("obstacle", [])
    core.memory("creep", [])

    for _ in range(10) : 
        core.memory("agents").append(Agents(
            Body(
                Fustrum(),
                random.randint(0,400),
                random.randint(0,400)
            )
        ))
    
    for _ in range (100) : 
        core.memory("creep").append(Creep(
            random.randint(0,400),
            random.randint(0,400)
        ))
    
    for _ in range (20) : 
        core.memory("obstacle").append(Obstacle(
            random.randint(0,400),
            random.randint(0,400)
    ))


    print("Setup END-----------")


def computePerception(agent):
    for agent in core.memory("agents") : 
        for obj in core.memory("agents"),core.memory("obstacle"),core.memory("creep") :
            if agent.body.fustrum.inside(obj) : 
                agent.listPersception.append(obj)


def computeDecision(agent):
    for agent in core.memory("agents") : 
        agent.update()


def applyDecision(agent):
    for agent in core.memory("agents") : 
        agent.body.move(agent.decision)


def run():
    core.cleanScreen()
    
    #Display
    for agent in core.memory("agents"):
        agent.show()
    
    for obstacle in core.memory("obstacle"):
        obstacle.show()

    for creep in core.memory("creep"):
        creep.show()
        
    for agent in core.memory("agents"):
        computePerception(agent)
        
    for agent in core.memory("agents"):
        computeDecision(agent)
    
    for agent in core.memory("agents"):
        applyDecision(agent)
    
    
    
    
     
core.main(setup, run)

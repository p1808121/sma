from pygame.math import Vector2
import random
import core

class Agents :



    def __init__(self,body = None):
        self.uuid = random.randint(0,999999999999999)
        self.body = body
        self.listPersception = []

        pass

    def filtre(self) : 
        creep = []
        petit = []
        gros = []
        obstacle = []
        for p in self.listPersception :
            if isinstance(p,"Creep"):
                creep.append(p)
            if isinstance(p,"Obstacle"):
                obstacle.append(p)
            if isinstance(p,"Agents"):
                if p.body.size >= self.body.size : 
                    gros.append(p)
                else :
                    petit.append(p)
        return creep,petit,gros,obstacle

    def update(self) : 
        creep,petit,gros,obstacle = self.filtre()
        Rep = Vector2(0,0)
        Att = Vector2(0,0)
        if gros != [] : 
            for g in gros : 
                Rep += self.body.position-g.position
                if len(gros) != 0:
                    Rep/=len(g)
            self.decision = Rep
            return
        else : 
            if creep != [] : 
                min = 10000
                proche = None
                for c in creep :
                    temp = self.body.position.distance_to(c.position)
                    if temp < min : 
                        min = temp
                        proche = c
                Att = proche.position - self.body.position
                self.decision = Att
                return
            else : 
                self.decision = Vector2(random.randint(-10,10),random.randint(-10,10))


        
    
    def show(self) : 
        core.Draw.circle(self.body.color,self.body.position,self.body.taille)


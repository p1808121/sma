from pygame.math import Vector2
import epidemie

class Fustrum :
    def __init__(self,parent = None ,r = 100):
        self.radius = r
        self.parent = parent

    #on met dans une liste tous les objet a porté
    def inside(self,obj) : 
        if hasattr(obj,"position") :
            if isinstance(obj.position,"Vector2") :
                if obj.position.distance_to(self.parent.position) < self.radius : 
                    return True
        return False
    
    def insideContagion(self,obj) : 
        if hasattr(obj,"position") :
            print("TEST")
            if isinstance(obj.position,"Vector2") :
                if obj.position.distance_to(self.parent.position) < epidemie.distContagion : 
                    return True
        return False
import random
from pygame.math import Vector2
import core
from agents import Agents
from body import Body
from fustrum import Fustrum
from item import Item

def setup():
    print("Setup START---------")
    core.fps = 30
    core.WINDOW_SIZE = [400, 400]

    #on crée les objets de notre simu
    core.memory("agents", [])
    core.memory("item", [])

    #initialisation des agents
    for _ in range(10) : 
        core.memory("agents").append(Agents(
            Body(
                Fustrum(),
                random.randint(0,400),
                random.randint(0,400), 
                0,
                0
            )
        ))
    
    for _ in range(2) : 
        core.memory("agents").append(Agents(
            Body(
                Fustrum(),
                random.randint(0,400),
                random.randint(0,400), 
                1,
                1
            )
        ))
    
    #initialisation des objets 
    for _ in range (0) : 
        core.memory("item").append(Item(
        ))


    print("Setup END-----------")

#on appel les fonctions perceptions
def computePerception(agent):
    for agent in core.memory("agents") : 
        for obj in core.memory("agents"),core.memory("item") :
            if agent.body.fustrum.inside(obj) : 
                agent.listPersception.append(obj)

def computePerceptionContamination(agent):
    for agent in core.memory("agents") : 
        for obj in core.memory("agents") :
            if agent.body.fustrum.insideContagion(obj) : 
                agent.body.listPersceptionContagion.append(obj)           

# on appel les fonction decision
def computeDecision(agent):
    for agent in core.memory("agents") : 
        agent.update()

#on appel les fonction action 
def applyDecision(agent):
    for agent in core.memory("agents") : 
        agent.body.move(agent.decision)

def updateBody(agent) : 
    for agent in core.memory("agents") : 
        agent.body.update()


def run():
    core.cleanScreen()
    
    #Display
    for agent in core.memory("agents"):
        agent.show()

    for item in core.memory("item"):
        item.show()
        
    for agent in core.memory("agents"):
        computePerception(agent)
    
    for agent in core.memory("agents"):
        computePerceptionContamination(agent)
    
    for agent in core.memory("agents"):
        updateBody(agent)
        
    for agent in core.memory("agents"):
        computeDecision(agent)
    
    for agent in core.memory("agents"):
        applyDecision(agent)
    
    
    
    
     
core.main(setup, run)

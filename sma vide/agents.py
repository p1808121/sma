from pygame.math import Vector2
import random
import core

class Agents :



    def __init__(self,body = None):
        self.uuid = random.randint(0,999999999999999)
        self.body = body
        self.listPersception = []
        

        pass

    def filtre(self) : 
        # liste d'objet final
        item = []
        petit = []
        gros = []
        # on parcours les objets vue
        for p in self.listPersception :
            if isinstance(p,"Item"):
                item.append(p)
            if isinstance(p,"Agents"):
                if p.body.size >= self.body.size : 
                    gros.append(p)
                else :
                    petit.append(p)
        return item,petit,gros
        #on retourne le tout

    #fonction decision
    def update(self) : 
        self.decision = Vector2(random.randint(-10,10),random.randint(-10,10))


        
    
    def show(self) : 
        if (self.body.etat == 0):
            core.Draw.circle((255,255,255),self.body.position,self.body.taille)
        if (self.body.etat == 1):
            core.Draw.circle((0,125,0),self.body.position,50)
            core.Draw.circle((0,255,0),self.body.position,self.body.taille)
        if (self.body.etat == 2):
            core.Draw.circle((0,0,255),self.body.position,self.body.taille)
        if (self.body.etat == 3):
            core.Draw.circle((30,30,30),self.body.position,self.body.taille)

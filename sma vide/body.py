from pygame.math import Vector2
import random
import epidemie

class Body :
    def __init__(self,fustrum,x,y,etat,tempsMaladi):
        self.position = Vector2(x,y)
        self.vitesse = Vector2()
        self.acceleration = Vector2()
        self.vMax = 0.1
        self.accMax = 5
        self.taille = 10
        self.fustrum = fustrum
        self.etat = etat # 0 pour saint, 1 pour infécté, 2 pour gérie, 3 décédé
        self.tempsIncub = 0
        self.tempsMaladi = tempsMaladi
        self.contagieux = 0 # 1 si contagieux 0 sinon
        self.listPersceptionContagion = []

    # deplacement
    def move (self,desision: Vector2):
        if desision.length()>self.accMax : 
            desision.scale_to_length(self.accMax)
        self.vitesse += desision

        if self.vitesse.length()>self.vMax :
            self.vitesse.scale_to_length(self.vMax)
        self.position += self.vitesse

    def update(self) : 

        if self.tempsMaladi != 0 : 
            self.tempsMaladi = self.tempsMaladi +1
            if self.tempsMaladi > epidemie.dureeAvContagion and self.contagieux != 1 : 
                self.contagieux = 1
            if self.tempsMaladi > epidemie.dureeAvDeces :
                if random.randint(1,100) <= epidemie.pourcentageDeces :
                    self.etat = 3
                    self.tempsMaladi = 0
                    self.contagieux = 0
                else : 
                    self.etat = 2
                    self.tempsMaladi = 0
                    self.contagieux = 0
        elif self.tempsIncub != 0 : 
            self.tempsIncub = self.tempsIncub +1
            if self.tempsIncub > epidemie.dureeIncub : 
                self.etat = 1
                self.tempsMaladi = 1
                self.tempsIncub = 0
        
        elif self.etat == 0 :
            if not self.listPersceptionContagion :
                for _ in self.listPersceptionContagion : 
                    if random.randint(1,100) <= epidemie.pourcentageContagion :
                        self.tempsIncub = self.tempsIncub +1

        
    
    def etat(self) : 
        return self.etat
    def taille(self) : 
        return self.taille
